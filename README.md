# ah_osconfig_limit_pi_writes

An Ansible role for limiting the number of writes to preserve the life of systems booted from microSD.

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| Ubuntu | Y | WORKING |

## Changelog

| **Date** | **Description** |
|---|---|
| 2022-01-13 | Initial version |
